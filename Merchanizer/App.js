/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Provider, connect } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { applyMiddleware, combineReducers, createStore } from 'redux';

import React from 'react';
import { YellowBox } from 'react-native';
import accountReducer from './src/reducers/account.reducer';
import axios from 'axios';
import { createAppContainer } from 'react-navigation';
import customerReducer from './src/reducers/customer.reducer';
import exportOrdersReducer from './src/reducers/export-order.reducer';
import importOrdersReducer from './src/reducers/import-order.reducer';
import { mainSwich } from './src/navigators/main-switch.navigator';
import moment from 'moment';
import ordersReducer from './src/reducers/order.reducer';
import productReducer from './src/reducers/product.reducer';
import supplierReducer from './src/reducers/supplier.reducer';
import thunk from 'redux-thunk';

YellowBox.ignoreWarnings(['Remote debugger']);
moment.locale('vi');

const client = axios.create({
  baseURL: 'http://localhost:8081',
  responseType: 'json',
});

const store = createStore(
  combineReducers({
    product: productReducer,
    supplier: supplierReducer,
    customer: customerReducer,
    order: ordersReducer,
    exportOrder: exportOrdersReducer,
    importOrder: importOrdersReducer,
    account: accountReducer,
  }),
  // applyMiddleware(axiosMiddleware(client)),
  applyMiddleware(thunk),
);

const AppContainer = createAppContainer(mainSwich);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <AppContainer />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});