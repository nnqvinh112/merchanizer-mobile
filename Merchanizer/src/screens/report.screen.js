import { BarChart, Grid, PieChart, XAxis } from 'react-native-svg-charts';
import { Button, Text } from 'react-native-elements';
import { Platform, StyleSheet, View } from 'react-native';
import React, { PureComponent } from 'react';

import { ExportOrderModel } from '../models/export-order.model';
import { ImportOrderModel } from '../models/import-order.model';
import { ProductModel } from '../models/product.model';
import { Text as SVGText } from 'react-native-svg';
import { ScrollView } from 'react-native-gesture-handler';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchBestSeller } from '../actions/product.action';
import { fetchExportOrders } from '../actions/export-order.action';
import { fetchImportOrders } from '../actions/import-order.action';
import moment from 'moment';

const styles = {
  barLegendBox: {
    width: 20,
    height: 20,
  },
  barLegendItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20,
  }
}

class ReportScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: 'Thống kê'
  });

  componentDidMount() {
    this.props.fetchBestSeller();
    this.props.fetchExportOrders();
    this.props.fetchImportOrders();
  }

  render() {
    const inColor = 'skyblue';
    const outColor = 'darkslateblue';
    const numOfMonth = 6;
    const numOfProduct = 5;

    /** @type {ExportOrderModel[]} */
    const exportOrders = (this.props.exportOrders || []);
    /** @type {ImportOrderModel[]} */
    const importOrders = (this.props.importOrders || []);

    const data = {
      in: exportOrders.slice(0, numOfMonth).map(i => i.totalCost),
      out: importOrders.slice(0, numOfMonth).map(i => i.totalCost),
    }

    const thisMonth = moment().month();
    const months = new Array(numOfMonth)
      .fill(thisMonth - numOfMonth + 1)
      .map((i, index) => ((i + index + 12) % 12) + 1);

    const recentMonthsData = [
      {
        data: data.in,
        svg: { fill: inColor },
      },
      {
        data: data.out,
        svg: { fill: outColor },
      }
    ];
    const thisMonthIn = exportOrders
      .filter(i => moment(i.createdTime).get('month') === thisMonth)
      .map(i => i.totalCost)
      .reduce((pre, cur) => pre + cur, 0);
    const thisMonthOut = importOrders
      .filter(i => moment(i.createdTime).get('month') === thisMonth)
      .map(i => i.totalCost)
      .reduce((pre, cur) => pre + cur, 0);

    const thisMonthTotal = thisMonthIn + thisMonthOut;

    const thisMonthData = [
      {
        key: 1,
        value: Math.round(thisMonthIn / thisMonthTotal * 100),
        svg: { fill: inColor },
      },
      {
        key: 2,
        value: Math.round(thisMonthOut / thisMonthTotal * 100),
        svg: { fill: outColor }
      },
    ]

    const PieLabels = ({ slices, height, width }) => {
      return slices.map((slice, index) => {
        const { labelCentroid, pieCentroid, data } = slice;
        return (
          <SVGText
            key={index}
            x={pieCentroid[0]}
            y={pieCentroid[1]}
            fill={'white'}
            textAnchor={'middle'}
            alignmentBaseline={'middle'}
            fontSize={24}
            stroke={'black'}
            strokeWidth={0.2}
          >
            {data.value}%
          </SVGText>
        )
      })
    }

    const colors = ['#ea4b8f', '#a3bf0a', '#29abe2', 'green', '#523563', 'teal'].slice(0, numOfProduct);
    /** @type {ProductModel[]} */
    const products = (this.props.products || []).slice(0, numOfProduct);
    const productLabels = products.map(i => i.name);
    const productSellRates = products.map(i => i.quantity);
    const productData = productSellRates.map((i, index) => ({
      key: index,
      value: i,
      svg: { fill: colors[index] },
    }));

    return (
      <ScrollView>
        <View style={{ marginBottom: 10 }}>
          <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Doanh thu tháng {thisMonth + 1}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.barLegendItem}>
              <View style={{ ...styles.barLegendBox, backgroundColor: inColor }}></View>
              <Text> Thu</Text>
            </View>
            <View style={styles.barLegendItem}>
              <View style={{ ...styles.barLegendBox, backgroundColor: outColor }}></View>
              <Text> Chi</Text>
            </View>
          </View>
          <PieChart
            style={{ height: 200 }}
            valueAccessor={({ item }) => item.value}
            outerRadius={'95%'}
            innerRadius={0}
            data={thisMonthData}
          >
            <PieLabels />
          </PieChart>
        </View>


        <View style={{ marginBottom: 10 }}>
          <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Doanh thu {numOfMonth} tháng gần nhất</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.barLegendItem}>
              <View style={{ ...styles.barLegendBox, backgroundColor: inColor }}></View>
              <Text> Thu</Text>
            </View>
            <View style={styles.barLegendItem}>
              <View style={{ ...styles.barLegendBox, backgroundColor: outColor }}></View>
              <Text> Chi</Text>
            </View>
          </View>
          <BarChart
            style={{ height: 200 }}
            data={recentMonthsData}
            contentInset={{ top: 30, bottom: 30 }}
            />
          <XAxis
            data={data.out}
            svg={{
              fill: 'grey',
              fontSize: 10,
            }}
            numberOfTicks={6}
            contentInset={{ left: 30, right: 30 }}
            formatLabel={(value, index) => `Tháng ${months[index]}`}
          />
        </View>


        <View style={{ marginBottom: 10 }}>
          <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Top {numOfProduct} sản phẩm bán chạy</Text>
          <PieChart
            style={{ height: 200 }}
            valueAccessor={({ item }) => item.value}
            outerRadius={'95%'}
            innerRadius={0}
            data={productData}
          >
          </PieChart>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', flexWrap: 'wrap' }}>
            {
              productData.map((i, index) => (
                <View style={styles.barLegendItem} key={index}>
                  <View style={{ ...styles.barLegendBox, backgroundColor: colors[index] }}></View>
                  <Text> {productLabels[index]}</Text>
                </View>
              ))
            }
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.product.repos,
    importOrders: state.importOrder.repos,
    exportOrders: state.exportOrder.repos,
  };
};
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchBestSeller,
  fetchExportOrders,
  fetchImportOrders,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ReportScreen);
