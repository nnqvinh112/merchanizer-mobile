import { EXPORT_ROUTE, IMPORT_ROUTE } from '../navigators/route.constant';
import { Platform, ScrollView, StyleSheet, Text, View } from 'react-native';
import React, { Component } from 'react';

import { ButtonModel } from '../models/button.model';
import { Color } from '../styles/theme.style';
import { Input } from 'react-native-elements';
import ProductListItemComponent from '../components/product-list-item.component';
import { ProductModel } from '../models/product.model';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchProducts } from '../actions/product.action';

class StorageScreen extends Component {
  state = {
    /** @type {ProductModel[]} */
    products: [],
    buttons: [],
    disabled: true,
    returnUrl: '',
    detailsUrl: '',
    detailsEditable: true,
    searchText: '',
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const returnUrl = this.props.navigation.getParam('returnUrl');
    switch (returnUrl) {
      case EXPORT_ROUTE.MAIN_SCREEN:
      case IMPORT_ROUTE.MAIN_SCREEN:
        this.setState({
          disabled: false,
          detailsUrl: IMPORT_ROUTE.PRODUCT_DETAILS_SCREEN,
          detailsEditable: false,
          buttons: this.initViewButton(),
        });
        break;
      default: // Main storage tab
        this.setState({
          buttons: this.initConfigButton(),
          detailsUrl: 'ProductDetails',
        });
    };

    this.setState({ returnUrl });
    this.props.fetchProducts()
  }

  onConfig(button, data) {
    const { navigation } = this.props;
    const { detailsUrl, detailsEditable } = this.state;
    navigation.navigate(detailsUrl, {
      product: data,
      editable: detailsEditable,
    });
  }

  onItemSelect(item) {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect(item);
    }
  }

  initViewButton() {
    return [
      new ButtonModel({
        type: 'clear',
        icon: {
          name: 'remove-red-eye',
          color: Color.primary,
          size: 20,
        }
      })
    ];
  }
  initConfigButton() {
    return [
      new ButtonModel({
        type: 'clear',
        icon: {
          name: 'settings',
          color: Color.primary,
          size: 20,
        }
      })
    ];
  }

  render() {
    const { buttons, disabled, searchText } = this.state;
    const {
      /** @type {ProductModel[]} */
      products
    } = this.props;
    return (
      <ScrollView>
        <Input
          leftIcon={{ name: 'search', color: Color.secondary }}
          containerStyle={{ marginBottom: 10, marginTop: 10 }}
          inputContainerStyle={{ borderWidth: 0.5, borderColor: Color.secondaryLight }}
          placeholder="Tìm theo tên SP..."
          onChangeText={(searchText) => this.setState({ searchText })}
          value={searchText}
        />
        <View style={{ paddingLeft: 10, paddingRight: 10 }}>
          {
            products
            .filter(i => !searchText || (i.name || '').toLowerCase().indexOf(searchText.toLowerCase()) >= 0)
            .map(i => (
              <ProductListItemComponent
                disabled={disabled}
                key={i.id} product={i}
                buttons={buttons}
                onButtonClick={(button, data) => this.onConfig(button, data)}
                onPress={() => this.onItemSelect(i)}
              />
            ))
          }
        </View>
      </ScrollView>
    );
  }

}

const mapStateToProps = state => {
  let storedRepositories = state.product.repos;
  return { products: storedRepositories };
};

const mapDispatchToProps = dispatch => bindActionCreators({ fetchProducts }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(StorageScreen);
