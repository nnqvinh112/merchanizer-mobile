import { Avatar, Button, ButtonGroup, Input, Overlay, Text } from 'react-native-elements';
import { GET_EXPORT_ORDERS, GET_IMPORT_ORDERS } from '../reducers/order.reducer';
import { Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import React, { Component } from 'react';

import { Color } from '../styles/theme.style';
import CurrencyComponent from '../components/currency.component';
import { DATETIME } from '../constants/format.constants';
import { ExportOrderModel } from '../models/export-order.model';
import { ImportOrderModel } from '../models/import-order.model';
import { ORDER_ROUTE } from '../navigators/route.constant';
import OrderFilterComponent from '../components/order-filter.component';
import { OrderModel } from '../models/order.model';
import { ScrollView } from 'react-native-gesture-handler';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchExportOrders } from '../actions/export-order.action';
import { fetchImportOrders } from '../actions/import-order.action';
import moment from 'moment';

const tabs = [GET_EXPORT_ORDERS, GET_IMPORT_ORDERS];

class OrderScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return ({
      title: 'Hóa đơn',
      headerRight: (
        <Button
          type='clear'
          icon={{ name: 'filter-list' }}
          onPress={navigation.getParam('onFilterClick')}
        />
      )
    });
  };

  state = {
    selectedIndex: 0,
    showFilter: false,
    filterData: {},
    /** @type {OrderModel[]} */
    orders: [],
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      onFilterClick: this.onFilterClick,
    });
    this.loadOrders(tabs[0]);
  }

  loadOrders(type) {
    switch (type) {
      case GET_EXPORT_ORDERS:
        return this.props.fetchExportOrders();
      case GET_IMPORT_ORDERS:
        return this.props.fetchImportOrders();
      default: return;
    }
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex, orders: [], filterData: {} });
    this.loadOrders(tabs[selectedIndex]);
  }

  onOrderClick(item) {
    this.props.navigation.navigate(ORDER_ROUTE.DETAILS_SCREEN, { order: item });
  }

  onFilterClick = () => {
    this.toggleFilter(true);
  }

  onFilterConfirm(filterData) {
    this.toggleFilter(false);
    this.setState({ filterData });
  }

  /**
   *
   * @param {OrderModel} item
   */
  isMatchFilter(item) {
    const { filterData } = this.state;
    if (!filterData) {
      return true;
    }
    const { employeeName, supplierName, customerName, status, date } = filterData;
    if (employeeName
      && item.createdBy
      && item.createdBy.name.toLowerCase().indexOf(employeeName.toLowerCase()) < 0) {
      return false;
    }
    if (customerName
      && item.customer
      && item.customer.name.toLowerCase().indexOf(customerName.toLowerCase()) < 0) {
      return false;
    }
    if (supplierName
      && item.supplier
      && item.supplier.name.toLowerCase().indexOf(supplierName.toLowerCase()) < 0) {
      return false;
    }
    if (status && !status[0] && item.status && item.status.isSuccessful) {
      return false;
    }
    if (date && !moment(date).isSame(moment(item.createdTime), 'day')) {
      return false;
    }
    if (status && !status[1] && item.status && item.status.isFailed) {
      return false;
    }
    return true;
  }

  toggleFilter(enable) {
    if (enable !== undefined) {
      return this.setState({ showFilter: enable });
    }
    const { showFilter } = this.state;
    return this.setState({ showFilter: !showFilter });
  }

  render() {
    const buttons = ['Bán hàng', 'Nhập hàng'];
    const { orders, loading } = this.props;
    const { selectedIndex, showFilter, filterData } = this.state;
    const asociateLabel = ['Tên KH', 'Tên NCC'][selectedIndex];
    const asociate = (item) => [item.customer, item.supplier][selectedIndex];
    const creatorLabel = ['Tên NV bán', 'Tên NV nhập'][selectedIndex];
    return (
      <View style={{ flex: 1 }}>
        <ButtonGroup
          onPress={(index) => this.updateIndex(index)}
          selectedIndex={selectedIndex}
          buttons={buttons}
        />
        <Overlay isVisible={showFilter} animated animationType="fade">
          <OrderFilterComponent
            layout={selectedIndex}
            value={filterData}
            onConfirm={(data) => this.onFilterConfirm(data)}
            onCancel={() => this.toggleFilter(false)}
          />
        </Overlay>

        {!loading && <ScrollView style={{ padding: 10 }}>
          {
            orders && orders
              .filter(i => this.isMatchFilter(i))
              .map((/** @type {ExportOrderModel} */ item, i) => (
                <TouchableOpacity
                  onPress={() => this.onOrderClick(item)}
                  activeOpacity={0.25} key={i}
                  style={{ borderWidth: 0.5, padding: 10, marginBottom: 5, flex: 1, flexDirection: 'row' }}
                >
                  <Avatar
                    rounded icon={{ name: item.status.icon }}
                    overlayContainerStyle={{ backgroundColor: item.status.color }}
                    containerStyle={{ marginRight: 10, alignSelf: 'center' }}
                  />
                  <View style={{ flex: 1 }}>
                    <Text>
                      <Text style={{ fontWeight: 'bold' }}>{asociateLabel}: </Text>
                      {asociate(item) && <Text>{asociate(item).name}</Text>}
                    </Text>
                    <Text>
                      <Text style={{ fontWeight: 'bold' }}>{creatorLabel}: </Text>
                      <Text>{item.createdBy.name}</Text>
                    </Text>
                    <Text>
                      <Text style={{ fontWeight: 'bold' }}>Trạng thái: </Text>
                      <Text>{item.status.label}</Text>
                    </Text>
                  </View>
                  <View style={{ alignItems: 'flex-end' }}>
                    <Text>{item.totalAmount} sản phẩm</Text>
                    <CurrencyComponent value={item.totalCost} />
                    <Text>{moment(item.createdTime).format(DATETIME.MOMENT)}</Text>
                  </View>
                </TouchableOpacity>
              ))
          }
        </ScrollView>}
      </View>
    );
  }
}

const mapStateToProps = state => {
  let storedRepositories = state.order.repos;
  return {
    loading: state.order.loading,
    orders: storedRepositories
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchImportOrders,
  fetchExportOrders,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(OrderScreen);
