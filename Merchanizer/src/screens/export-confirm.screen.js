import { Button, Input, Text } from 'react-native-elements';
import React, {Component} from 'react';

import AutocompleteComponent from '../components/autocomplete.component';
import { Color } from '../styles/theme.style';
import CurrencyComponent from '../components/currency.component';
import { CustomerModel } from '../models/customer.model';
import { EXPORT_ROUTE } from '../navigators/route.constant';
import ProductListItemComponent from '../components/product-list-item.component';
import { ProductModel } from '../models/product.model';
import { ScrollView } from 'react-native-gesture-handler';
import {View} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchCustomers } from '../actions/customer.action';
import moment from 'moment';

class ExportConfirmScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    title: 'Tạo hóa đơn bán hàng'
  });

  state = {
    customer: new CustomerModel(),
    /** @type {CustomerModel[]} */
    customers: [],
  }

  componentWillMount() {
    this.props.fetchCustomers();
  }

  onConfirm() {
    const {navigation} = this.props;
    navigation.navigate(EXPORT_ROUTE.MAIN_SCREEN);
    navigation.state.params.onResetForm();
  }

  /**
   * @return {ProductModel[]}
   */
  getProducts() {
    const { navigation } = this.props;
    const products = navigation.getParam('products');
    return products || [];
  }

  getTotalValue() {
    return this.getProducts()
      .map(i => i.quantity * i.price)
      .reduce((previous, current) => previous + current, 0);
  }

  getTotalItems() {
    return this.getProducts()
      .map(i => i.quantity)
      .reduce((previous, current) => previous + current, 0);
  }

  updateCustomer(value, field) {
    if (typeof(value) ===  'object') {
      return this.setState({customer: value});
    }
    const {customer} = this.state;
    customer[field] = value;
    this.setState({customer});
  }

  render() {
    const { navigation } = this.props;
    const { customer, customers } = this.state;
    const products = navigation.getParam('products');
    return customer && (
      <View style={{ padding: 10, flex: 1}}>
        <ScrollView>
          <View style={{marginBottom: 20}}>
            <Text h4 style={{marginLeft: 'auto', marginRight: 'auto'}}>Chi tiết hóa đơn</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{color: Color.primary}}>Ngày tạo hóa đơn</Text>
              <Text style={{color: Color.primary}}>{moment().format('DD/MM/YYYY HH:mm')}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{color: Color.primary}}>Tổng sản phẩm</Text>
              <Text style={{color: Color.primary}}>{this.getTotalItems()} sản phẩm</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
              <Text style={{color: Color.primary}}>Tổng giá tiền</Text>
              <CurrencyComponent style={{color: Color.primary}} value={this.getTotalValue()}/>
            </View>
            <View>
              {
                products.map((i, index) => (
                  <ProductListItemComponent disabled product={i} key={index}/>
                ))
              }
            </View>
          </View>
          <View style={{marginBottom: 60}}>
            <Text h4 style={{marginLeft: 'auto', marginRight: 'auto'}}>Khách hàng</Text>
            <Text style={{marginLeft: 'auto', marginRight: 'auto', fontSize: 16}}>(Không bắt buộc)</Text>
            <Input
              label='Tên khách hàng'
              placeholder='Tên khách hàng'
              onChangeText={(value) => this.updateCustomer(value, 'name')}
              value={customer.name}
              />
            <AutocompleteComponent
              data={customers}
              modelValueField='identity'
              modelLabelField='name'
              rightLabelFields={['genderLabel', 'birthDay']}
              inputLabel='CMND'
              inputKeyboardType='numeric'
              onChangeText={value => this.updateCustomer(value, 'identity')}
              onSelect={(item) => this.updateCustomer(item)}
            />
            <Input
              label='Địa chỉ'
              placeholder='Địa chỉ'
              onChangeText={(value) => this.updateCustomer(value, 'address')}
              value={customer.address}
              />
            <Input
              label='Email'
              placeholder='Email'
              onChangeText={(value) => this.updateCustomer(value, 'email')}
              value={customer.email}
              />
            <Input
              label='Số điện thoại'
              placeholder='Số điện thoại'
              keyboardType='numeric'
              onChangeText={(value) => this.updateCustomer(value, 'phone')}
              value={customer.phone}
              />
            {/* <AutocompleteComponent
              data={customers}
              modelValueField='email'
              modelLabelField='name'
              rightLabelFields={['genderLabel', 'birthDay']}
              inputLabel='Email'
              onChangeText={value => this.updateCustomer(value, 'email')}
              onSelect={(item) => this.updateCustomer(item)}
            />
            <AutocompleteComponent
              data={customers}
              modelValueField='phone'
              modelLabelField='name'
              rightLabelFields={['genderLabel', 'birthDay']}
              inputLabel='Số điện thoại'
              inputKeyboardType='numeric'
              onChangeText={value => this.updateCustomer(value, 'phone')}
              onSelect={(item) => this.updateCustomer(item)}
            /> */}
          </View>
        </ScrollView>
        <Button
          title="Xác nhận"
          onPress={() => this.onConfirm()}
          containerStyle={{marginTop: 'auto'}} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  let storedRepositories = state.customer.repos;
  return {
    repos: storedRepositories
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ fetchCustomers }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ExportConfirmScreen);
