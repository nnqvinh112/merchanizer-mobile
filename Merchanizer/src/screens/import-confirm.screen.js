import { Button, Input, Text } from 'react-native-elements';
import { Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import React, { Component } from 'react';

import Autocomplete from 'react-native-autocomplete-input';
import AutocompleteComponent from '../components/autocomplete.component';
import { Color } from '../styles/theme.style';
import CurrencyComponent from '../components/currency.component';
import { IMPORT_ROUTE } from '../navigators/route.constant';
import { ImportOrderModel } from '../models/import-order.model';
import ProductListItemComponent from '../components/product-list-item.component';
import { ProductModel } from '../models/product.model';
import { ScrollView } from 'react-native-gesture-handler';
import { SupplierModel } from '../models/supplier.model';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchSuppliers } from '../actions/supplier.action';
import moment from 'moment';
import { postImportOrder } from '../actions/import-order.action';

class ImportConfirmScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Tạo hóa đơn nhập hàng'
  });

  state = {
    supplier: new SupplierModel(),
    /** @type {SupplierModel[]} */
    suppliers: [],
  }

  componentWillMount() {
    this.props.fetchSuppliers();
  }

  onConfirm() {
    const { navigation, supplier } = this.props;
    // this.props.postImportOrder()
    const order = {
      products: this.getProducts(),
      supplier
    };

    this.props.postImportOrder(order);
    navigation.navigate(IMPORT_ROUTE.MAIN_SCREEN);
    navigation.state.params.onResetForm();
  }

  isProcessible() {
    const { supplier } = this.state;
    return !!supplier.taxCode;
  }
  /**
   * @return {ProductModel[]}
   */
  getProducts() {
    const { navigation } = this.props;
    const products = navigation.getParam('products');
    return products || [];
  }

  getTotalValue() {
    return this.getProducts()
      .map(i => i.quantity * i.price)
      .reduce((previous, current) => previous + current, 0);
  }

  getTotalItems() {
    return this.getProducts()
      .map(i => i.quantity)
      .reduce((previous, current) => previous + current, 0);
  }

  getFilteredSupliers() {

  }

  updateSupplier(value, field) {
    if (typeof (value) === 'object') {
      return this.setState({ supplier: value });
    }
    const { supplier } = this.state;
    supplier[field] = value;
    this.setState({ supplier });
  }

  render() {
    const { suppliers } = this.props;
    const { supplier } = this.state;
    const products = this.getProducts();
    return supplier && (
      <View style={{ padding: 10, flex: 1 }}>
        <ScrollView>
          <View style={{ marginBottom: 20 }}>
            <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Nhà cung cấp</Text>
            <Input
              label='Tên nhà cung cấp'
              placeholder='Tên nhà cung cấp'
              onChangeText={(value) => this.updateSupplier(value, 'name')}
              value={supplier.name}
            />
            <AutocompleteComponent
              data={suppliers}
              modelValueField='taxCode'
              modelLabelField='name'
              inputLabel='Mã số thuế'
              inputKeyboardType='numeric'
              required
              onChangeText={value => this.updateSupplier(value, 'taxCode')}
              onSelect={(item) => this.updateSupplier(item)}
            />
            <Input
              label='Địa chỉ'
              placeholder='Địa chỉ'
              onChangeText={(value) => this.updateSupplier(value, 'address')}
              value={supplier.address}
            />
            {/* <Input
              label='Email'
              placeholder='Email'
              onChangeText={(value) => this.updateSupplier(value, 'email')}
              value={supplier.email}
            /> */}
            <Input
              label='Số điện thoại'
              placeholder='Số điện thoại'
              keyboardType='numeric'
              onChangeText={(value) => this.updateSupplier(value, 'phone')}
              value={supplier.phone}
            />
          </View>

          <View style={{ marginBottom: 20 }}>
            <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Chi tiết hóa đơn</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={{ color: Color.primary }}>Ngày tạo hóa đơn</Text>
              <Text style={{ color: Color.primary }}>{moment().format('DD/MM/YYYY HH:mm')}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={{ color: Color.primary }}>Tổng sản phẩm</Text>
              <Text style={{ color: Color.primary }}>{this.getTotalItems()} sản phẩm</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
              <Text style={{ color: Color.primary }}>Tổng giá tiền</Text>
              <CurrencyComponent style={{ color: Color.primary }} value={this.getTotalValue()} />
            </View>
            <View>
              {
                products.map((i, index) => (
                  <ProductListItemComponent disabled product={i} key={index} />
                ))
              }
            </View>
          </View>

        </ScrollView>
        <Button
          title="Xác nhận"
          disabled={!this.isProcessible()}
          onPress={() => this.onConfirm()}
          containerStyle={{ marginTop: 'auto' }} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  let storedRepositories = state.supplier.repos;
  return {
    suppliers: storedRepositories
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ fetchSuppliers, postImportOrder }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ImportConfirmScreen);
