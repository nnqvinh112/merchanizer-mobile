
import React, {Component} from 'react';
import {View, ScrollView, TouchableOpacity} from 'react-native';
import { Button, Icon, Text, Avatar } from 'react-native-elements';
import InsertListItemComponent from '../components/insert-list-item.component';
import { ProductModel } from '../models/product.model';
import { IMPORT_ROUTE } from '../navigators/route.constant';
import ProductListItemComponent from '../components/product-list-item.component';
import CurrencyComponent from '../components/currency.component';
import { Color } from '../styles/theme.style';
import { ButtonModel } from '../models/button.model';
import Swipeable from 'react-native-swipeable';

const BUTTON = {
  INCREASE: 'increase',
  DECREASE: 'decrease',
}

export default class ImportScreen extends Component {
  state = {
    /** @type {ProductModel[]} */
    products: [],
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: 'Nhập hàng',
      headerRight: (
          <Button
            type="clear"
            onPress={navigation.getParam('onResetForm')}
            title="Tạo mới"
            icon={{ name:"note-add", color: Color.primary }}
          />
        )
    };
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { navigation } = this.props;
    navigation.setParams({
      onResetForm: this.onResetForm,
    });
    // this.props.getProducts().then(res => {
    //   if (res.type === GET_PRODUCTS_SUCCESS) {
    //     this.setState({products: res.payload.data});
    //   }
    // });
  }

  onResetForm = () => {
    this.setState({ products: [] });
  }

  /**
   * Listener for quantity increasing/decreasing button
   * @param {ButtonModel} button
   * @param {ProductModel} data
   */
  onQuantityChange(button, data) {
    const { products } = this.state;
    const existedProduct = products.find(x => x.id === data.id);
    if (existedProduct) {
      switch (button.data) {
        case BUTTON.INCREASE:
          existedProduct.quantity += 1;
          break;
        case BUTTON.DECREASE:
          if (existedProduct.quantity > 0) {
            existedProduct.quantity -= 1;
          }
          break;
      }
    }
    this.setState({ products });
  }

  onSelect = data => {
    const { products } = this.state;
    const newProduct = new ProductModel({
      ...data,
      quantity: 1
    });
    const existedProduct = products.find(x => x.id === newProduct.id);
    if (existedProduct) {
      existedProduct.quantity += newProduct.quantity;
    } else {
      products.push(newProduct);
    }
    this.setState({ products });
  };

  onAddNew() {
    this.props.navigation.navigate(IMPORT_ROUTE.STORAGE_SCREEN, {
      returnUrl: IMPORT_ROUTE.MAIN_SCREEN,
      onSelect: this.onSelect,
    });
  }

  onRemove(item) {
    const { products } = this.state;
    const index = products.findIndex(x => x.id === item.id);
    products.splice(index, 1);
    this.setState({ products });
  }

  onCreateOrder() {
    this.props.navigation.navigate(IMPORT_ROUTE.CONFIRM, {
      onResetForm: this.onResetForm,
      products: this.state.products,
    });
  }

  isProcessible() {
    return this.getTotalProducts() > 0;
  }

  getProcessibleProducts() {
    const { products } = this.state;
    return (products|| []).filter(x => x.quantity > 0);
  }

  getTotalValue() {
    const { products } = this.state;
    return (products || [])
      .map(i => i.quantity * i.price)
      .reduce((previous, current) => previous + current, 0);
  }

  getTotalProducts() {
    return this.getProcessibleProducts()
      .map(i => i.quantity)
      .reduce((previous, current) => previous + current, 0);
  }

  renderConfigButton() {
    return [
      new ButtonModel({
        data: BUTTON.INCREASE,
        type: 'clear',
        icon: {
          name: 'add',
          color: Color.primary,
          size: 20,
        }
      }),
      new ButtonModel({
        data: BUTTON.DECREASE,
        type: 'clear',
        icon: {
          name: 'remove',
          color: Color.primary,
          size: 20,
        }
      })
    ];
  }

  renderSwipeButtons(item) {
    return [
      <TouchableOpacity
        style={{flex: 1, backgroundColor: Color.danger, justifyContent: 'center', alignItems: 'flex-start'}}
        activeOpacity={0.25}
        onPress={() => this.onRemove(item)}
      >
        <Avatar size="large" icon={{ name: 'delete' }} overlayContainerStyle={{backgroundColor: 'transparent'}} />
      </TouchableOpacity>
    ];
  };

  render() {
    const {products} = this.state;
    return (
      <View style={{ padding: 10, flex: 1}}>
        <ScrollView style={{marginBottom: 20}}>
          <Text style={{marginLeft: 'auto', marginRight: 'auto'}} h4>Giỏ hàng</Text>
          <InsertListItemComponent onPress={() => this.onAddNew()}/>
          <View>
          {
            products.map(i => (
              <Swipeable key={i.id} rightButtons={this.renderSwipeButtons(i)}>
                <ProductListItemComponent
                  disabled
                  product={i}
                  buttons={this.renderConfigButton()}
                  onButtonClick={(button, data) => this.onQuantityChange(button, data)}
                />
              </Swipeable>
            ))
          }
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{ fontWeight: '500'}}>Tổng cộng:</Text>
            <Text style={{ marginLeft: 'auto'}}>
              <CurrencyComponent value={this.getTotalValue()}/>
              <Text> - </Text>
              <Text>{this.getTotalProducts()} sản phẩm</Text>
            </Text>
          </View>
        </ScrollView>
        <Button
          title="Tạo hóa đơn"
          disabled={!this.isProcessible()}
          onPress={() => this.onCreateOrder()}
          containerStyle={{marginTop: 'auto'}} />
      </View>
    );
  }
}
