import { Button, Input, Text } from 'react-native-elements';
import { Platform, ScrollView, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

import { Color } from '../styles/theme.style';
import CurrencyComponent from '../components/currency.component';
import { OrderModel } from '../models/order.model';
import ProductListItemComponent from '../components/product-list-item.component';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchExportOrder } from '../actions/export-order.action';
import { fetchImportOrder } from '../actions/import-order.action';
import moment from 'moment';

styles = {
  labelColumn: {
    color: Color.primary,
    fontWeight: 'bold',
    maxWidth: '35%',
    textAlign: 'left',
  },
  valueColumn: {
    color: Color.primary,
    maxWidth: '65%',
    textAlign: 'right',
  }
}

class OrderDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Chi tiết hóa đơn'
  });

  state = {
    layout: 0,
    /** @type {OrderModel} */
    order: null,
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const order = this.props.navigation.getParam('order');

    let layout = 0;
    if (order.customer) {
      layout = 0;
      this.props.fetchExportOrder(order.id);
    } else {
      layout = 1;
      this.props.fetchImportOrder(order.id);
    }

    this.setState({ order, layout });
  }

  render() {
    const { layout } = this.state;
    const { order, loading } = this.props;
    if (!order) {
      return <View></View>;
    }
    return (
      <View style={{ padding: 10, flex: 1 }}>
        <ScrollView>
          {/* Export view */}
          {layout === 0 && order.customer && (
            <View style={{ marginBottom: 20 }}>
              <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Khách hàng</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Khách hàng</Text>
                <Text style={styles.valueColumn}>{order.customer.name}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Ngày sinh</Text>
                <Text style={styles.valueColumn}>{
                  order.customer.birthDay
                    ? moment(order.customer.birthDay).format('DD/MM/YYYY')
                    : ''
                }</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Giới tính</Text>
                <Text style={styles.valueColumn}>{order.customer.genderLabel}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Địa chỉ</Text>
                <Text style={styles.valueColumn}>{order.customer.address}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Email</Text>
                <Text style={styles.valueColumn}>{order.customer.email}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Số điện thoại</Text>
                <Text style={styles.valueColumn}>{order.customer.phone}</Text>
              </View>
            </View>
          )}
          {/* Import view */}
          {layout === 1 && order.supplier && (
            <View style={{ marginBottom: 20 }}>
              <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Nhà cung cấp</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Nhà cung cấp</Text>
                <Text style={styles.valueColumn}>{order.supplier.name}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Mã số thuế</Text>
                <Text style={styles.valueColumn}>{order.supplier.taxCode}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Địa chỉ</Text>
                <Text style={styles.valueColumn}>{order.supplier.address}</Text>
              </View>
              {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Email</Text>
                <Text style={styles.valueColumn}>{order.supplier.email}</Text>
              </View> */}
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.labelColumn}>Số điện thoại</Text>
                <Text style={styles.valueColumn}>{order.supplier.phone}</Text>
              </View>
            </View>
          )}
          <View style={{ marginBottom: 20 }}>
            <Text h4 style={{ marginLeft: 'auto', marginRight: 'auto' }}>Chi tiết hóa đơn</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.labelColumn}>Ngày tạo hóa đơn</Text>
              <Text style={styles.valueColumn}>{moment(order.createdTime).format('DD/MM/YYYY HH:mm')}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.labelColumn}>Tạo bởi</Text>
              <Text style={styles.valueColumn}>{order.createdBy.name}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.labelColumn}>Tổng sản phẩm</Text>
              <Text style={styles.valueColumn}>{order.totalAmount} sản phẩm</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
              <Text style={styles.labelColumn}>Tổng giá tiền</Text>
              <CurrencyComponent style={styles.valueColumn} value={order.totalCost} />
            </View>
            <View>
              { !loading &&
                (order.products || []).map((i, index) => (
                  <ProductListItemComponent disabled product={i} key={index} />
                ))
              }
            </View>
          </View>
        </ScrollView>
        {/* <Button
          title="Xác nhận"
          disabled={!this.isProcessible()}
          onPress={() => this.onConfirm()}
          containerStyle={{marginTop: 'auto'}} /> */}
      </View>
    );
  }
}

const mapStateToProps = state => {
  let storedRepositories = state.order.repo;
  return {
    loading: state.order.loading,
    order: storedRepositories
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchExportOrder,
  fetchImportOrder,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailsScreen);
