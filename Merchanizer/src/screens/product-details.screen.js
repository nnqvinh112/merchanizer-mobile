import { Avatar, Button, Input } from 'react-native-elements';
import {Platform, ScrollView, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';

import { Color } from '../styles/theme.style';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ProductModel } from '../models/product.model';

const styles = {
  inputDisabled: {
    backgroundColor: 'lightgray',
  }
}
export default class ProductDetailsScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    title: 'Chi tiết sản phẩm'
  });

  constructor(props) {
    super(props);
  }

  componentWillMount() {

  }

  render() {
    const { navigation } = this.props;
    /** @type {ProductModel} */
    const product = navigation.getParam('product');
    /** @type {boolean} */
    const editable = navigation.getParam('editable');
    console.log("TCL: render -> product", product)
    if (!product) {
      return null;
    }
    return (
      <ScrollView>
        <View style={{ marginTop: 20 , marginBottom: 20, width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Avatar
            containerStyle={{borderWidth: 2, borderColor: Color.secondaryLight}}
            size="xlarge"
            rounded
            source={{
              uri: product.imageUrl,
            }}
            // showEditButton={editable}
            showEditButton={false}
          />

        </View>
        <Input
          editable={false}
          inputStyle={styles.inputDisabled}
          label='Mã sản phẩm'
          placeholder='Chưa cập nhật'
          value={product.id}
          />
        <Input
          editable={editable}
          label='Tên sản phẩm'
          placeholder='Chưa cập nhật'
          value={product.name}
          />
        <Input
          editable={editable}
          label='Đơn giá'
          placeholder='Chưa cập nhật'
          keyboardType='numeric'
          value={product.price + ''}
          />
        <Input
          editable={editable}
          label='Số lượng tồn kho'
          placeholder='Chưa cập nhật'
          keyboardType='numeric'
          value={product.quantity + ''}
          />

        <Input
          editable={false}
          label='Nhà cung cấp'
          placeholder='Chưa cập nhật'
          inputStyle={styles.inputDisabled}
          value={product.supplier.name}
        />
        {
          editable && (
            <Button title='Cập nhật' containerStyle={{paddingLeft: 10, paddingRight: 10, marginTop: 10, marginBottom: 10}}/>
          )
        }
      </ScrollView>
    );
  }
}
