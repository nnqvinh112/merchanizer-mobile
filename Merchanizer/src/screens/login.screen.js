import { Avatar, Button, Input, Text } from 'react-native-elements';
import { Platform, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

import { Color } from '../styles/theme.style';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { login } from '../actions/account.action';

class LoginScreen extends Component {
  state = {
    username: '',
    password: '',
    usernameError: '',
    passwordError: '',
  }

  isValidate() {
    const { password, username } = this.state;
    if (username.length <= 0) {
      this.setState({ usernameError: 'Vui lòng nhập tài khoản' })
      return false;
    }
    this.setState({ usernameError: '' })

    if (password.length === 0) {
      this.setState({ passwordError: 'Vui lòng nhập mật khẩu' })
      return false;
    }
    if (password.length < 8) {
      this.setState({ passwordError: 'Mật khẩu phải có ít nhất 8 ký tự' })
      return false;
    }
    return username.length > 0 && password.length >= 8;
  }

  async login() {
    if (!this.isValidate()) {
      return;
    }
    const { password, username } = this.state;
    const res = await this.props.login(username, password);
    if (!res) {
      this.setState({ passwordError: 'Sai tài khoản hoặc mật khẩu.' });
      return;
    }
    return this.props.navigation.navigate('App');
  };

  render() {
    const { password, username, passwordError, usernameError } = this.state;
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <Text style={{ textAlign: 'center', fontSize: 18 }}>Quản lý cửa hàng tối ưu hơn với</Text>
        <Text h3 style={{ color: Color.primary, textAlign: 'center' }}>Merchanizer</Text>
        <View style={{ marginTop: '20%', width: '80%', padding: 20, borderWidth: 1, borderRadius: 15, borderColor: 'lightgray', backgroundColor: '#ffffff22' }}>
          <View style={{}}>
            <Avatar
              rounded size="large" icon={{ name: 'lock' }}
              containerStyle={{ marginTop: -50, alignSelf: 'center' }}
            />
            <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>Đăng nhập để tiếp tục</Text>
            <Input
              autoFocus
              value={username}
              placeholder="Tài khoản"
              errorMessage={usernameError}
              onChangeText={username => this.setState({ username })}
            />
            <Input
              value={password}
              placeholder="Mật khẩu"
              secureTextEntry={true}
              onChangeText={password => this.setState({ password })}
              errorMessage={passwordError}
            />
          </View>
          <Button title="Đăng nhập" onPress={() => this.login()} buttonStyle={{ margin: 10 }} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  let storedRepositories = state.account.repo;
  return { repo: storedRepositories };
};

const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
