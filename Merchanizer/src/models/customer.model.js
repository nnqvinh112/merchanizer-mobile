import { PersonModel } from "./person.model";

export class CustomerModel extends PersonModel {
    /** @type {Date} */
    lastVisitedTime = null;

    constructor(item) {
        super(item);

        if (item) {
            this.id = item.id || item.idCus;
            this.name = item.name || item.nameCus;
            this.lastVisitedTime = item.lastVisitedTime;
        } else {
            this.name = 'Khách lẻ';
        }
    }
}