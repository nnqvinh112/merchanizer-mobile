import { EmployeeModel } from "./employee.model";
import { ImportOrderDetailsModel } from "./import-order-details.model";
import { OrderModel } from "./order.model";
import { OrderStatusModel } from "./order-status.model";
import { SupplierModel } from "./supplier.model";

export class ImportOrderModel extends OrderModel {
  /** @type {SupplierModel} */
  supplier;
  /** @type {EmployeeModel} */
  employee;
  details = [];

  constructor(item) {
    super(item);

    if (item) {
      this.id = item.id || item.idIo;
      this.supplier = new SupplierModel(item.supplier|| item.suppliedByNavigation);
      this.createdBy = new EmployeeModel(item.employee || item.createdByNavigation);
      this.status = new OrderStatusModel(item.status || item.statusIo);
      this.details = (item.iodetail || []).map(i => new ImportOrderDetailsModel(i));
      this.products = this.details.map(i => {
        i.product.quantity = i.amount;
        i.product.price = i.cost;
        return i.product;
      });
    }
  }
}