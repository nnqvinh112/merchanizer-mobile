import { BaseModel } from "./base.model";
import { EmployeeModel } from "./employee.model";
import { OrderStatusModel } from "./order-status.model";
import { ProductModel } from "./product.model";

export class OrderModel extends BaseModel {
  totalCost = 0;
  totalAmount = 0;
  /** @type {OrderStatusModel} */
  status = null;

  /** @type {EmployeeModel} */
  createdBy = null;

  /** @type {ProductModel[]} */
  products = [];

  constructor(item) {
    super(item);

    if (item) {
      this.totalAmount = item.totalAmount;
      this.totalCost = item.totalCost;

      this.status = new OrderStatusModel(item.status);
      this.createdBy = new EmployeeModel(item.createdBy);
      this.products = (item.products || []).map(i => new ProductModel(i));
    }
  }
}