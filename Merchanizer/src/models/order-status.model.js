import { BaseModel } from "./base.model";
import { Color } from "../styles/theme.style";

export class OrderStatusModel extends BaseModel {
  id = '';

  constructor(item) {
    super(item);

    if (item) {
      if (typeof(item) === 'string') {
        this.id = item;
      } else {
        this.id = item.id;
      }
    }
  }

  get icon() {
    switch (this.id) {
      case '1':
        return 'check';
      case '2':
        return 'block';
      default:
        return 'warning';
    }
  }

  get color() {
    switch (this.id) {
      case '1':
        return Color.success;
      case '2':
        return Color.danger;
      default:
        return Color.primaryDark;
    }
  }

  get label() {
    switch (this.id) {
      case '1':
        return 'Hoàn tất';
      case '2':
        return 'Bị hủy';
      default:
        return 'Bị lỗi';
    }
  }

  get isSuccessful() {
    return this.id === '1';
  }

  get isFailed() {
    return this.id === '2';
  }
}