import { ContractInfoModel } from "./contract-info.model";

export class PersonModel extends ContractInfoModel {
    birthDay = null;
    gender = '';
    genderLabel = '';
    name = '';
    identity = '';

    constructor(item) {
        super(item);

        if (item) {
            this.name = item.name;
            this.birthDay = item.birthDay;
            this.identity = item.identity || item.identify;
            this.gender = item.gender;
            switch (this.gender) {
                case 'f':
                case 'female':
                case 'nu':
                case 'nữ':
                    this.genderLabel = 'Nữ';
                    break;
                case 'm':
                case 'male':
                case 'nam':
                    this.genderLabel = 'Nam';
                    break;
                default:
                    this.genderLabel = 'Khác';
            }
        }
    }
}