export class BaseModel {
  id = '';
  createdTime = new Date();
  updatedTime = new Date();

  constructor(item) {
    if (item) {
      this.id = item.id;
      this.createdTime = item.createdTime || new Date();
      this.updatedTime = item.updatedTime || new Date();
    } else {
      this.createdTime = new Date();
      this.updatedTime = new Date();
    }
  }
}