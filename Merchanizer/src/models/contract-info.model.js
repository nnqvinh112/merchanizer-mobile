import { BaseModel } from "./base.model";

export class ContractInfoModel extends BaseModel {
    address = '';
    phone = '';
    email = '';
    name = '';

    constructor(item) {
        super(item);
    
        if (item) {
            this.name = item.name;
            this.address = item.address;
            this.phone = item.phone;
            this.email = item.email;
        }
    }
}