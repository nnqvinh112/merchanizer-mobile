import { CustomerModel } from "./customer.model";
import { EmployeeModel } from "./employee.model";
import { ExportOrderDetailsModel } from "./export-order-details.model";
import { OrderModel } from "./order.model";
import { OrderStatusModel } from "./order-status.model";

export class ExportOrderModel extends OrderModel {
  totalDiscount = 0;
  /** @type {CustomerModel} */
  customer;
  /** @type {ExportOrderDetailsModel[]} */
  details = [];

  constructor(item) {
    super(item);

    if (item) {
      this.id = item.id || item.idEo;

      this.totalDiscount = item.totalDiscount;
      this.customer = new CustomerModel(item.idCusNavigation || item.customer);
      this.status = new OrderStatusModel(item.status || item.statusEo);
      this.createdBy = new EmployeeModel(item.employee || item.createdByNavigation);
      this.details = (item.eodetail || []).map(i =>  new ExportOrderDetailsModel(i));
      this.products = this.details.map(i => {
        i.product.quantity = i.amount;
        i.product.price = i.cost;
        return i.product;
      });
    }
  }
}