import { BaseModel } from "./base.model";
import { SupplierModel } from './supplier.model';

const defaultImage = 'https://www.lauriloewenberg.com/wp-content/uploads/2019/04/No_Image_Available.jpg';

export class ProductModel extends BaseModel {
  name = '';
  price = 0;
  quantity = 0;
  imageUrl = defaultImage;
  category;
  /** @type {SupplierModel} */
  supplier;

  constructor(item) {
    super(item);

    if (item) {
      this.name = item.name || item.namePro;
      this.quantity = item.quantity;
      this.price = item.price;
      this.category = item.category;
      this.imageUrl = item.imageUrl || defaultImage;
      this.supplier = new SupplierModel(item.supplier);

      this.id = item.id || item.idPro;
    }
  }
}