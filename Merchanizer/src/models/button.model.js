export class ButtonModel {
  title = '';
  type = '';
  icon = {
    name: '',
    color: '',
    size: 10,
  }
  style = {};
  loading = false;
  data;

  constructor(item) {
    if (item) {
      this.title = item.title;
      this.type = item.type;
      this.icon = item.icon;
      this.style = item.style;
      this.loading = item.loading;
      this.data = item.data;
    }
  }
}