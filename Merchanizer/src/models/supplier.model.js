import { ContractInfoModel } from "./contract-info.model";

export class SupplierModel extends ContractInfoModel {
    taxCode = '';

    constructor(item) {
        super(item);

        if (item) {
          this.taxCode = item.taxCode || item.taxcode;
          this.taxCode = this.taxCode + '';
        }
    }
}