import { BaseModel } from "./base.model";
import { ProductModel } from "./product.model";

export class ImportOrderDetailsModel extends BaseModel {
    amount = 0;
    cost = 0;
    product;

    constructor(item) {
        super(item);
        if (item) {
            this.amount = item.amount;
            this.cost = item.cost;
            this.product = new ProductModel(item.product || item.infoProductNavigation);
        }
    }
}