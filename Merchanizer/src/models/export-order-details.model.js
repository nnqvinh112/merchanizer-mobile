import { BaseModel } from "./base.model";
import { ProductModel } from "./product.model";

export class ExportOrderDetailsModel extends BaseModel {
    amount = 0;
    discount = 0;
    cost = 0;
    /** @type {ProductModel} */
    product;

    constructor(item) {
        super(item);
        if (item) {
            this.amount = item.amount;
            this.discount = item.discount;
            this.cost = item.cost;
            this.product = new ProductModel(item.product || item.infoProductNavigation);
        }
    }
}