import { PersonModel } from "./person.model";
// import { ImportOrderModel } from "./import-order.model";
// import { ExportOrderModel } from "./export-order.model";

export class EmployeeModel extends PersonModel {
    role = '';
    exportOrders = [];
    importOrders = [];

    constructor(item) {
        super(item);

        if (item) {
            this.id = item.id || item.idEm;
            this.name = item.name || item.nameEm;
            this.role = item.role;
            // this.importOrders = (item.importOrder || []).map(i => new ImportOrderModel(i));
            // this.exportOrders = (item.exportOrder || []).map(i => new ExportOrderModel(i));
        }
    }
}