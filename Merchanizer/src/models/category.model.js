import { BaseModel } from "./base.model";

export class CategoryModel extends BaseModel {
  name = '';

  constructor(item) {
    super(item);

    if (item) {
      this.name = item.name;
    }
  }
}