import { createStackNavigator } from 'react-navigation';
import { mainTab } from './main-tab.navigator';

export const mainStack = createStackNavigator({
    MainTab: mainTab,
}, {
    headerMode: 'none',
});