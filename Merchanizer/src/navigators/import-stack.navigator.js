import { createStackNavigator } from "react-navigation";
import ProductDetailsScreen from "../screens/product-details.screen";
import { Icon } from "react-native-elements";
import React from 'react';
import ImportScreen from "../screens/import.screen";
import StorageScreen from "../screens/storage.screen";
import { IMPORT_ROUTE } from "./route.constant";
import ImportConfirmScreen from "../screens/import-confirm.screen";

export const ImportStack = createStackNavigator({
  [IMPORT_ROUTE.MAIN_SCREEN]: ImportScreen,
  [IMPORT_ROUTE.STORAGE_SCREEN]: StorageScreen,
  [IMPORT_ROUTE.PRODUCT_DETAILS_SCREEN]: ProductDetailsScreen,
  [IMPORT_ROUTE.CONFIRM]: ImportConfirmScreen,
}, {
  navigationOptions: ({navigation}) => ({
    tabBarLabel: 'Nhập hàng',
    tabBarIcon: <Icon name='call-received'/>,
  }),
  defaultNavigationOptions: ({navigation}) => {
    const { routeName } = navigation.state;
    let title = '';
    switch (routeName) {
      case IMPORT_ROUTE.STORAGE_SCREEN:
        title = 'Danh sách sản phẩm';
        break;
    }
    return ({ title })
  },
});