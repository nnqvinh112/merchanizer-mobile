import { ExportStack } from './export-stack.navigator';
import { ImportStack } from './import-stack.navigator';
import { OrderStack } from './order-stack.navigator';
import { ReportStack } from './report-stack.navigator';
import { StorageStack } from './storage-stack.navigator';
import {
    createBottomTabNavigator
} from 'react-navigation';

export const mainTab = createBottomTabNavigator({
    Report: ReportStack,
    Order: OrderStack,
    Storage: StorageStack,
    Import: ImportStack,
    Export: ExportStack,
}, {
    initialRouteName: 'Storage',
});