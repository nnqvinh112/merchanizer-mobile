import { createStackNavigator } from "react-navigation";
import { Icon } from "react-native-elements";
import React from 'react';
import { Color } from "../styles/theme.style";
import { REPORT_ROUTE } from "./route.constant";
import ReportScreen from "../screens/report.screen";

export const ReportStack = createStackNavigator({
  [REPORT_ROUTE.MAIN_SCREEN]: ReportScreen,
}, {
  navigationOptions: ({navigation}) => ({
    tabBarLabel: 'Thống kê',
    tabBarIcon: <Icon name='assessment'/>
  }),
});