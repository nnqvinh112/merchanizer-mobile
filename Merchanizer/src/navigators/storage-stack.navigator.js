import { Button, Icon, Text } from "react-native-elements";

import { Color } from "../styles/theme.style";
import ProductDetailsScreen from "../screens/product-details.screen";
import React from 'react';
import StorageScreen from "../screens/storage.screen";
import { createStackNavigator } from "react-navigation";
import { logOut } from "../actions/account.action";

export const StorageStack = createStackNavigator({
  Storage: StorageScreen,
  ProductDetails: ProductDetailsScreen,
}, {
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: 'Kho hàng',
      tabBarIcon: <Icon name='store' />
    }),
    defaultNavigationOptions: ({ navigation }) => {
      return ({
        title: 'Kho hàng',
        headerRight: (<Button
          onPress={() => {
            logOut();
            navigation.navigate('Auth');
          }}
          type="clear"
          icon={
            <Icon
              type="font-awesome"
              name="sign-out"
              size={15}
            />
          }
        />)
      })
    },
  });