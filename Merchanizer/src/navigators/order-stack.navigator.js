import { createStackNavigator } from "react-navigation";
import OrderScreen from "../screens/order.screen";
import { Icon } from "react-native-elements";
import React from 'react';
import { Color } from "../styles/theme.style";
import { ORDER_ROUTE } from "./route.constant";
import OrderDetailsScreen from "../screens/order-details.screen";

export const OrderStack = createStackNavigator({
  [ORDER_ROUTE.MAIN_SCREEN]: OrderScreen,
  [ORDER_ROUTE.DETAILS_SCREEN]: OrderDetailsScreen,
}, {
  navigationOptions: ({navigation}) => ({
    tabBarLabel: 'Hóa đơn',
    tabBarIcon: <Icon name='assignment'/>
  }),
});