import { createSwitchNavigator } from "react-navigation";
import { mainStack } from "./main-stack.navigator";
import AuthLoadingScreen from "../screens/auth-loading.screen";
import { authStack } from "./auth-stack.navigator";

export const mainSwich = createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: mainStack,
        Auth: authStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
);