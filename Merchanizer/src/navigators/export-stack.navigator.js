import { createStackNavigator } from "react-navigation";
import ProductDetailsScreen from "../screens/product-details.screen";
import { Icon } from "react-native-elements";
import React from 'react';
import StorageScreen from "../screens/storage.screen";
import { EXPORT_ROUTE } from "./route.constant";
import ExportScreen from "../screens/export.screen";
import ExportConfirmScreen from "../screens/export-confirm.screen";

export const ExportStack = createStackNavigator({
  [EXPORT_ROUTE.MAIN_SCREEN]: ExportScreen,
  [EXPORT_ROUTE.STORAGE_SCREEN]: StorageScreen,
  [EXPORT_ROUTE.PRODUCT_DETAILS_SCREEN]: ProductDetailsScreen,
  [EXPORT_ROUTE.CONFIRM]: ExportConfirmScreen,
}, {
  navigationOptions: ({navigation}) => ({
    tabBarLabel: 'Bán hàng',
    tabBarIcon: <Icon name='call-made'/>,
  }),
  defaultNavigationOptions: ({navigation}) => {
    const { routeName } = navigation.state;
    let title = '';
    switch (routeName) {
      case EXPORT_ROUTE.STORAGE_SCREEN:
        title = 'Danh sách sản phẩm';
        break;
    }
    return ({ title })
  },
});