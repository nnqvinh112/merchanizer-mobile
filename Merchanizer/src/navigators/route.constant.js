
export const IMPORT_ROUTE = {
  MAIN_SCREEN: 'Import/Main',
  STORAGE_SCREEN: 'Import/Storage',
  CONFIRM: 'Import/Confirm',
  PRODUCT_DETAILS_SCREEN: 'Import/ProductDetails',
};

export const EXPORT_ROUTE = {
  MAIN_SCREEN: 'Export/Main',
  STORAGE_SCREEN: 'Export/Storage',
  CONFIRM: 'Export/Confirm',
  PRODUCT_DETAILS_SCREEN: 'Export/ProductDetails',
};

export const STORAGE_ROUTE = {
  MAIN_SCREEN: 'Storage/Main',
  PRODUCT_DETAILS_SCREEN: 'Storage/ProductDetails',
};

export const ORDER_ROUTE = {
  MAIN_SCREEN: 'Order/Main',
  DETAILS_SCREEN: 'Order/Details',
};

export const REPORT_ROUTE = {
  MAIN_SCREEN: 'Report/Main',
};