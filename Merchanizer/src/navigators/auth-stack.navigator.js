import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../screens/login.screen';

export const authStack = createStackNavigator({
    LoginScreen: LoginScreen,
}, {
    headerMode: 'none',
});