export const Color = {
  primary: '#2089dc',
  primaryDark: '#1274c1',
  primaryLight: '#eff7fd',
  secondary: '#484c4e',
  secondaryLight: '#c6cbd1',
  light: '#ffffff',
  danger: '#da1111',
  success: '#0cdc30',
}