export const API = {
  HOST: '',
  PRODUCT: {
    ALL: 'http://localhost:5000/api/Products',
    BEST_SELLERS: 'http://localhost:5000/api/Products/bestSeller',
  },
  EXPORT_ORDER: {
    ALL: 'http://localhost:5000/api/ExportOrders',
  },
  IMPORT_ORDER: {
    ALL: 'http://localhost:5000/api/ImportOrders',
  },
  SUPPLIER: {
    ALL: 'http://localhost:5000/api/Suppliers',
  },
  CUSTOMER: {
    ALL: 'http://localhost:5000/api/Customers',
  },
  EMPLOYEE: {
    ALL: 'http://localhost:5000/api/Employees',
  },
  ACCOUNT: {
    ALL: 'http://localhost:5000/api/Accounts',
  },
}