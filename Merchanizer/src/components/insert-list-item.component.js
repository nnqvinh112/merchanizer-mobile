
import React, { Component } from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {Text, Button, Icon} from 'react-native-elements';
import { Color } from '../styles/theme.style';

const imageSize = 75;
const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderColor: Color.secondaryLight,
    backgroundColor: Color.primary,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  image: {
    width: imageSize,
    height: imageSize,
    borderWidth: 0.5,
    borderColor: Color.secondaryLight,
    backgroundColor: Color.primaryDark,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    padding: 8,
    flex: 1,
  },
  contentTitle: {
    fontWeight: '500',
    fontSize: 16,
    color: 'white',
    marginTop: 'auto',
    marginBottom: 'auto',
  },
});

class InsertListItemComponent extends Component {
  state = {  }
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={styles.container}>
            <View style={styles.image}>
              <Icon
                    name="add"
                    size={50}
                    color="white"
                  />
            </View>
            <View style={styles.content}>
              <Text style={styles.contentTitle}>Thêm sản phẩm</Text>
            </View>
          </View>
        </TouchableOpacity>
    );
  }
}

export default InsertListItemComponent;