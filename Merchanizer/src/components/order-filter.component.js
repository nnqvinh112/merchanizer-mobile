import { Button, ButtonGroup, Icon, Input, Text } from 'react-native-elements';
import { CheckBox, Platform, ScrollView, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

import { Color } from '../styles/theme.style';
import DatePicker from 'react-native-datepicker';

const styles = {
  radioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  }
}
export default class OrderFilterComponent extends Component {
  state = {
    date: '',
    status: [true, true],
    customerName: '',
    employeeName: '',
    supplierName: '',
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.updateFilter();
  }

  updateFilter() {
    if (this.props.value) {
      const { supplierName, employeeName, customerName, status, date } = this.props.value;
      this.setState({
        supplierName, employeeName, customerName, status: status || [true, true], date
      })
    }
  }

  onConfirm() {
    if (this.props.onConfirm) {
      this.props.onConfirm(this.state);
    }
  }

  onCancel() {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  }

  onReset() {
    if (this.props.onConfirm) {
      this.props.onConfirm();
    }
  }

  updateCheckbox(index, value) {
    const { status } = this.state;
    status[index] = value;
    this.setState({ status });
  }

  render() {
    const { supplierName, employeeName, customerName, status, date } = this.state;
    const { layout } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          <Text h4 h4Style={{ fontSize: 16 }}>Tên nhân viên</Text>
          <Input
            leftIcon={{ name: 'search', color: Color.secondary }}
            containerStyle={{ marginBottom: 10, marginTop: 10 }}
            inputContainerStyle={{ borderWidth: 0.5, borderColor: Color.secondaryLight }}
            placeholder="Tìm theo tên nhân viên"
            value={employeeName}
            onChangeText={(employeeName) => this.setState({ employeeName })}
          />
          {
            layout == 0 && (
              <View>
                <Text h4 h4Style={{ fontSize: 16 }}>Tên khách hàng</Text>
                <Input
                  leftIcon={{ name: 'search', color: Color.secondary }}
                  containerStyle={{ marginBottom: 10, marginTop: 10 }}
                  inputContainerStyle={{ borderWidth: 0.5, borderColor: Color.secondaryLight }}
                  placeholder="Tên khách hàng"
                  value={customerName}
                  onChangeText={(customerName) => this.setState({ customerName })}
                />
              </View>
            )}
          {layout == 1 && (
            <View>
              <Text h4 h4Style={{ fontSize: 16 }}>Tên nhà cung cấp</Text>
              <Input
                leftIcon={{ name: 'search', color: Color.secondary }}
                containerStyle={{ marginBottom: 10, marginTop: 10 }}
                inputContainerStyle={{ borderWidth: 0.5, borderColor: Color.secondaryLight }}
                placeholder="Tên nhà cung cấp"
                value={supplierName}
                onChangeText={(supplierName) => this.setState({ supplierName })}
              />
            </View>
          )}

          <Text h4 h4Style={{ fontSize: 16 }}>Trạng thái</Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
            <View style={styles.radioContainer}>
              <CheckBox
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                value={status[0]}
                onValueChange={(value) => this.updateCheckbox(0, value)}
              />
              <Text>Hoàn tất</Text>
            </View>
            <View style={styles.radioContainer}>
              <CheckBox
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                onValueChange={(value) => this.updateCheckbox(1, value)}
                value={status[1]}
              />
              <Text>Bị hủy</Text>
            </View>
            {/* <View style={styles.radioContainer}>
              <CheckBox
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                checked={false}
              />
              <Text>Bị lỗi</Text>
            </View> */}
          </View>

          <Text h4 h4Style={{ fontSize: 16 }}>Ngày tạo hóa đơn</Text>
          <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>
            <DatePicker
              style={{ width: '100%', borderWidth: 0.5 }}
              date={this.state.date}
              mode="date"
              placeholder="Chọn ngày"
              format="DD/MM/YYYY"
              iconComponent={(<Icon name="date-range" />)}
              maxDate={new Date()}
              customStyles={{
                dateInput: {
                  borderWidth: 0
                },
              }}
              onDateChange={(date) => { this.setState({ date: date }) }}
            />
          </View>

        </ScrollView>
        <View style={{ marginTop: 'auto', flexDirection: 'row', justifyContent: 'flex-end', padding: 10, borderTopWidth: 0.5 }}>
          <Button title="Bỏ lọc" type="outline" onPress={() => this.onReset()} containerStyle={{ marginRight: 'auto' }} />
          <Button title="Trở lại" type="outline" containerStyle={{ marginRight: 10 }} onPress={() => this.onCancel()} />
          <Button title="Lọc" onPress={() => this.onConfirm()} />
        </View>
      </View>
    );
  }
}
