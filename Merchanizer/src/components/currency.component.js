import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { Text } from 'react-native-elements';

export default class CurrencyComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <NumberFormat
        value={this.props.value}
        suffix={this.props.suffix || 'đ'}
        displayType={'text'}
        thousandSeparator={true}
        renderText={value => <Text style={this.props.style}>{value}</Text>}/>
    )
  }
}