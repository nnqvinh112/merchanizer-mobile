import { Input, Text } from 'react-native-elements';
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';

import Autocomplete from 'react-native-autocomplete-input';

export default class AutocompleteComponent extends Component {
    state = {
        value: '',
        data: [],
        focused: false,
        selectedItem: null,
        dirty: true,
    }
    constructor(props) {
        super(props);
    }

    componentDidMount() { }

    onSelect(selectedItem) {
        const { modelValueField, onSelect } = this.props;
        this.setState({ selectedItem, value: selectedItem[modelValueField] });
        if (onSelect) {
            onSelect(Object.assign({}, selectedItem));
        }
    }

    onTextChange(value) {
        const { onChangeText } = this.props;
        this.setState({ value });
        this.setFilterData();
        if (onChangeText) {
            onChangeText(value);
        }
    }

    getFilterData() {
        const { value } = this.state;
        const { data, modelValueField } = this.props;
        return (data || []).filter(x => `${x[modelValueField]}`.toLowerCase().indexOf(`${value}`.toLowerCase()) >= 0);
    }

    setFilterData() {
        this.setState({
            data: this.getFilterData(),
        });
    }

    isListHidden() {
        const { value, selectedItem, focused } = this.state;
        const { modelValueField } = this.props;
        return !value || (selectedItem && value === selectedItem[modelValueField]);
    }

    render() {
        const { value, data } = this.state;
        const {
            modelValueField, modelLabelField, defaultValue,
            inputLabel, inputPlaceholder, inputKeyboardType, inputContainerStyle, required,
            rightLabelFields,
            itemContainerStyle,
            renderItem, renderTextInput,
            errorMessage,
        } = this.props;

        return (
            <View style={{ position: 'relative' }}>
                <Autocomplete
                    onFocus={() => this.setState({ focused: true })}
                    data={this.getFilterData()}
                    defaultValue={defaultValue}
                    hideResults={this.isListHidden()}
                    inputContainerStyle={{ borderWidth: 0, ...inputContainerStyle }}
                    listContainerStyle={{ position: 'absolute', top: '100%', zIndex: 1, width: '100%' }}
                    keyExtractor={(_item, i) => i.toString()}
                    renderItem={renderItem || (({ item, index }) => (
                        <TouchableOpacity
                            key={index}
                            onPress={() => this.onSelect(item)}
                            style={{ backgroundColor: 'white', padding: 5, borderWidth: 0.5, flex: 1, flexDirection: 'row', ...itemContainerStyle }}
                        >
                            <View>
                                <Text>{item[modelLabelField]}</Text>
                                <Text style={{ fontStyle: 'italic' }}>{item[modelValueField]}</Text>
                            </View>
                            {
                                rightLabelFields && (
                                    <View style={{marginLeft: 'auto', alignItems: 'flex-end', justifyContent: 'flex-start', textAlign: 'right'}}>
                                        {rightLabelFields.map((field, fIndex) => (
                                            <Text key={fIndex}>{item[field]}</Text>
                                        ))}
                                    </View>
                                )
                            }
                        </TouchableOpacity>
                    ))}
                    renderTextInput={renderTextInput || (() => (
                        <Input
                            label={inputLabel}
                            placeholder={inputPlaceholder || inputLabel}
                            keyboardType={inputKeyboardType}
                            onChangeText={(value) => this.onTextChange(value)}
                            value={value}
                            errorMessage={errorMessage || (required && !value) ? inputLabel + ' không được rỗng' : ''}
                        />
                    ))}
                />
            </View>
        )
    }
}