import {Button, Text} from 'react-native-elements';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import React, { Component } from 'react';

import { Color } from '../styles/theme.style';
import CurrencyComponent from './currency.component';
import { ProductModel } from '../models/product.model';

const imageSize = 75;
const styles = StyleSheet.create({
  container: {
    // borderRadius: 4,
    borderWidth: 0.5,
    borderColor: Color.secondaryLight,
    marginBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    overflow: 'hidden',
  },
  image: {
    width: imageSize,
    height: imageSize,
    borderWidth: 0.5,
    borderColor: Color.secondaryLight,
  },
  content: {
    padding: 8,
    flex: 1,
  },
  contentTitle: {
    fontWeight: '500',
    fontSize: 16,
  },
  buttons: {
    width: 45,
    height: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class ProductListItemComponent extends Component {
  state = {  }
  constructor(props) {
    super(props);
  }

  onClick(button) {
    const {onButtonClick, product} = this.props;
    if (onButtonClick) {
      onButtonClick(button, product);
    }
  }

  onPress() {
    if (!this.props.disabled && this.props.onPress) {
      this.props.onPress();
    }
  }

  render() {
    const {
      /** @type {ProductModel} */
      product,
      buttons,
      disabled,
    } = this.props;
    if (!product) {
      return null;
    }
    return (
      <TouchableOpacity style={styles.container} activeOpacity={disabled ? 1 : 0.25} onPress={() => this.onPress()}>
        <View style={styles.image}>
          <Image style={styles.image} source={{uri: product.imageUrl}}></Image>
        </View>
        <View style={styles.content}>
          <Text style={styles.contentTitle}>{product.name}</Text>
          <Text>
            <CurrencyComponent value={product.price}/>
            <Text> x </Text>
            <Text>{product.quantity}</Text>
          </Text>
        </View>
        {buttons && (
          <View style={styles.buttons}>
            {buttons.map((i, index) => (
              <Button
                key={index}
                type={i.type}
                icon={i.icon ? {
                  name: i.icon.name,
                  size: i.icon.size || 15,
                  color: i.icon.color
                } : {}}
                title={i.title}

                onPress={() => this.onClick(i)}
              />
            ))}
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

export default ProductListItemComponent;