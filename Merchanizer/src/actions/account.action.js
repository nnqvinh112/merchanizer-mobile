import {
  ACCOUNT_LOGIN,
  ACCOUNT_LOGIN_FAIL,
  ACCOUNT_LOGIN_SUCCESS
} from "../reducers/account.reducer";

import { API } from "../constants/api.constants";
import AsyncStorage from "@react-native-community/async-storage";

function loginPending() {
  return {
    type: ACCOUNT_LOGIN
  }
}

function loginSuccess(payload) {
  return {
    type: ACCOUNT_LOGIN_SUCCESS,
    payload: payload
  }
}

function loginError(error) {
  return {
    type: ACCOUNT_LOGIN_FAIL,
    error: error
  }
}

export function login(username, password) {
  return dispatch => {
    dispatch(loginPending());
    return fetch(API.ACCOUNT.ALL, {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
      .then(res => res.json())
      .then(res => {
        AsyncStorage.setItem('userToken', JSON.stringify(res));
        dispatch(loginSuccess(res));
        return res;
      })
      .catch(error => {
        dispatch(loginError(error));
        return null;
      })
  }
}

export function logOut() {
  AsyncStorage.clear();
}
