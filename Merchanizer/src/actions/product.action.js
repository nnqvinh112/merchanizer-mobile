import { GET_BEST_SELLERS, GET_BEST_SELLERS_FAIL, GET_BEST_SELLERS_SUCCESS, GET_PRODUCTS, GET_PRODUCTS_FAIL, GET_PRODUCTS_SUCCESS } from "../reducers/product.reducer";

import { API } from "../constants/api.constants";
import { ProductModel } from "../models/product.model";

function fetchAllPending() {
    return {
        type: GET_PRODUCTS
    }
}

function fetchAllSuccess(payload) {
    return {
        type: GET_PRODUCTS_SUCCESS,
        payload: payload
    }
}

function fetchAllError(error) {
    return {
        type: GET_PRODUCTS_FAIL,
        error: error
    }
}

function fetchBestSellerPending() {
    return {
        type: GET_BEST_SELLERS
    }
}

function fetchBestSellerSuccess(payload) {
    return {
        type: GET_BEST_SELLERS_SUCCESS,
        payload: payload
    }
}

function fetchBestSellerError(error) {
    return {
        type: GET_BEST_SELLERS_FAIL,
        error: error
    }
}

export function fetchProducts() {
    return dispatch => {
        dispatch(fetchAllPending());
        fetch(API.PRODUCT.ALL)
            .then(res => res.json())
            .then(res => {
                dispatch(fetchAllSuccess((res || []).map(i => new ProductModel(i))));
                return res;
            })
            .catch(error => {
                console.error("TCL: fetchProducts -> error", error)
                dispatch(fetchAllError(error));
            })
    }
}

export function fetchBestSeller() {
    return dispatch => {
        dispatch(fetchBestSellerPending());
        fetch(API.PRODUCT.BEST_SELLERS)
            .then(res => res.json())
            .then(res => {
                dispatch(fetchBestSellerSuccess((res || []).map(i => new ProductModel(i))));
                return res;
            })
            .catch(error => {
                console.error("TCL: fetchBestSeller -> error", error)
                dispatch(fetchBestSellerError(error));
            })
    }
}
