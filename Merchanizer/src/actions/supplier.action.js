import {
    GET_SUPPLIERS,
    GET_SUPPLIERS_FAIL,
    GET_SUPPLIERS_SUCCESS
} from "../reducers/supplier.reducer";

import { API } from "../constants/api.constants";
import { SupplierModel } from "../models/supplier.model";

function fetchAllPending() {
    return {
        type: GET_SUPPLIERS
    }
}

function fetchAllSuccess(payload) {
    return {
        type: GET_SUPPLIERS_SUCCESS,
        payload: payload
    }
}

function fetchAllError(error) {
    return {
        type: GET_SUPPLIERS_FAIL,
        error: error
    }
}

export function fetchSuppliers() {
    return dispatch => {
        dispatch(fetchAllPending());
        fetch(API.SUPPLIER.ALL)
            .then(res => res.json())
            .then(res => {
                dispatch(fetchAllSuccess(res.map(i => new SupplierModel(i))));
                return res;
            })
            .catch(error => {
                console.error("TCL: fetchProducts -> error", error)
                dispatch(fetchAllError(error));
            })
    }
}
