import {
    GET_CUSTOMERS,
    GET_CUSTOMERS_FAIL,
    GET_CUSTOMERS_SUCCESS
} from "../reducers/customer.reducer";

import { API } from "../constants/api.constants";
import { CustomerModel } from "../models/customer.model";

function fetchAllPending() {
    return {
        type: GET_CUSTOMERS
    }
}

function fetchAllSuccess(payload) {
    return {
        type: GET_CUSTOMERS_SUCCESS,
        payload: payload
    }
}

function fetchAllError(error) {
    return {
        type: GET_CUSTOMERS_FAIL,
        error: error
    }
}

export function fetchCustomers() {
    return dispatch => {
        dispatch(fetchAllPending());
        fetch(API.CUSTOMER.ALL)
            .then(res => res.json())
            .then(res => {
                dispatch(fetchAllSuccess(res.map(i => new CustomerModel(i))));
                return res;
            })
            .catch(error => {
                console.error("TCL: fetchCustomers -> error", error)
                dispatch(fetchAllError(error));
            })
    }
}
