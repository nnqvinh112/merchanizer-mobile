import {
  GET_IMPORT_ORDER,
  GET_IMPORT_ORDERS,
  GET_IMPORT_ORDERS_FAIL,
  GET_IMPORT_ORDERS_SUCCESS,
  GET_IMPORT_ORDER_SUCCESS,
  POST_IMPORT_ORDER,
  POST_IMPORT_ORDER_FAIL,
  POST_IMPORT_ORDER_SUCCESS
} from "../reducers/order.reducer";

import { API } from "../constants/api.constants";
import { ImportOrderModel } from "../models/import-order.model";

function fetchAllPending() {
  return {
    type: GET_IMPORT_ORDERS
  }
}

function fetchAllSuccess(payload) {
  return {
    type: GET_IMPORT_ORDERS_SUCCESS,
    payload: payload
  }
}

function fetchAllError(error) {
  return {
    type: GET_IMPORT_ORDERS_FAIL,
    error: error
  }
}

function fetchOnePending() {
  return {
    type: GET_IMPORT_ORDER
  }
}

function fetchOneSuccess(payload) {
  return {
    type: GET_IMPORT_ORDER_SUCCESS,
    payload: payload
  }
}

function fetchOneError(error) {
  return {
    type: GET_IMPORT_ORDER_FAIL,
    error: error
  }
}

function postOnePending() {
  return {
    type: POST_IMPORT_ORDER
  }
}

function postOneSuccess(payload) {
  return {
    type: POST_IMPORT_ORDER_SUCCESS,
    payload: payload
  }
}

function postOneError(error) {
  return {
    type: POST_IMPORT_ORDER_FAIL,
    error: error
  }
}

export function fetchImportOrders() {
  return dispatch => {
    dispatch(fetchAllPending());
    fetch(API.IMPORT_ORDER.ALL)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchAllSuccess(res.map(i => new ImportOrderModel(i))));
        return res;
      })
      .catch(error => {
        console.error("TCL: fetchImportOrders -> error", error)
        dispatch(fetchAllError(error));
      })
  }
}

export function fetchImportOrder(id) {
  return dispatch => {
    dispatch(fetchOnePending());
    fetch(`${API.IMPORT_ORDER.ALL}/${id}`)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchOneSuccess(new ImportOrderModel(res)));
      })
      .catch(error => {
        console.error("TCL: fetchImportOrder -> error", error)
        dispatch(fetchOneError(error));
      })
  }
}

export function postImportOrder(order) {
  console.log("TCL: postImportOrder -> order", order)
  return dispatch => {
    dispatch(fetchOnePending());
    fetch(API.IMPORT_ORDER.ALL, { method: 'POST', body: order })
      .then(res => res.json())
      .then(res => {
        console.log("TCL: postImportOrder -> res", res, new ImportOrderModel(res))
        dispatch(fetchOneSuccess(new ImportOrderModel(res)));
      })
      .catch(error => {
        console.error("TCL: postImportOrder -> error", error)
        dispatch(fetchOneError(error));
      })
  }
}