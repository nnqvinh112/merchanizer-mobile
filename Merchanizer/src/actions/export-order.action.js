import {
  GET_EXPORT_ORDER,
  GET_EXPORT_ORDERS,
  GET_EXPORT_ORDERS_FAIL,
  GET_EXPORT_ORDERS_SUCCESS,
  GET_EXPORT_ORDER_FAIL,
  GET_EXPORT_ORDER_SUCCESS
} from "../reducers/order.reducer";

import { API } from "../constants/api.constants";
import { ExportOrderModel } from "../models/export-order.model";

function fetchAllPending() {
  return {
    type: GET_EXPORT_ORDERS
  }
}

function fetchAllSuccess(payload) {
  return {
    type: GET_EXPORT_ORDERS_SUCCESS,
    payload: payload
  }
}

function fetchAllError(error) {
  return {
    type: GET_EXPORT_ORDERS_FAIL,
    error: error
  }
}

function fetchOnePending() {
  return {
    type: GET_EXPORT_ORDER
  }
}

function fetchOneSuccess(payload) {
  return {
    type: GET_EXPORT_ORDER_SUCCESS,
    payload: payload
  }
}

function fetchOneError(error) {
  return {
    type: GET_EXPORT_ORDER_FAIL,
    error: error
  }
}

export function fetchExportOrders() {
  return dispatch => {
    dispatch(fetchAllPending());
    fetch(API.EXPORT_ORDER.ALL)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchAllSuccess(res.map(i => new ExportOrderModel(i))));
      })
      .catch(error => {
        console.error("TCL: fetchExportOrders -> error", error)
        dispatch(fetchAllError(error));
      })
  }
}

export function fetchExportOrder(id) {
  return dispatch => {
    dispatch(fetchOnePending());
    fetch(`${API.EXPORT_ORDER.ALL}/${id}`)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchOneSuccess(new ExportOrderModel(res)));
      })
      .catch(error => {
        console.error("TCL: fetchExportOrders -> error", error)
        dispatch(fetchOneError(error));
      })
  }
}