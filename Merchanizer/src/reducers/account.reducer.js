export const ACCOUNT_LOGIN = 'ACCOUNT/LOGIN';
export const ACCOUNT_LOGIN_SUCCESS = 'ACCOUNT/LOGIN_SUCCESS';
export const ACCOUNT_LOGIN_FAIL = 'ACCOUNT/LOGIN_FAIL';

export default function accountReducer(state = { repos: [] }, action) {
  switch (action.type) {
    case ACCOUNT_LOGIN:
      return { ...state, loading: true };
    case ACCOUNT_LOGIN_SUCCESS:
      return { ...state, loading: false, repo: action.payload };
    case ACCOUNT_LOGIN_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}
