export const GET_SUPPLIERS = 'SUPPLIERS/LOAD';
export const GET_SUPPLIERS_SUCCESS = 'SUPPLIERS/LOAD_SUCCESS';
export const GET_SUPPLIERS_FAIL = 'SUPPLIERS/LOAD_FAIL';

export default function supplierReducer(state = { repos: [] }, action) {
  switch (action.type) {
    case GET_SUPPLIERS:
      return { ...state, loading: true };
    case GET_SUPPLIERS_SUCCESS:
      return { ...state, loading: false, repos: action.payload };
    case GET_SUPPLIERS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}
