export const GET_CUSTOMERS = 'CUSTOMERS/LOAD';
export const GET_CUSTOMERS_SUCCESS = 'CUSTOMERS/LOAD_SUCCESS';
export const GET_CUSTOMERS_FAIL = 'CUSTOMERS/LOAD_FAIL';

export default function customerReducer(state = { repos: [] }, action) {
  switch (action.type) {
    case GET_CUSTOMERS:
      return { ...state, loading: true };
    case GET_CUSTOMERS_SUCCESS:
      return { ...state, loading: false, repos: action.payload };
    case GET_CUSTOMERS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}
