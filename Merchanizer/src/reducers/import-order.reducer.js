export const GET_IMPORT_ORDERS = 'IMPORT_ORDERS/LOAD';
export const GET_IMPORT_ORDERS_SUCCESS = 'IMPORT_ORDERS/LOAD_SUCCESS';
export const GET_IMPORT_ORDERS_FAIL = 'IMPORT_ORDERS/LOAD_FAIL';

export const GET_IMPORT_ORDER = 'IMPORT_ORDER/LOAD';
export const GET_IMPORT_ORDER_SUCCESS = 'IMPORT_ORDER/LOAD_SUCCESS';
export const GET_IMPORT_ORDER_FAIL = 'IMPORT_ORDER/LOAD_FAIL';

export const POST_IMPORT_ORDER = 'IMPORT_ORDER/POST';
export const POST_IMPORT_ORDER_SUCCESS = 'IMPORT_ORDER/POST_SUCCESS';
export const POST_IMPORT_ORDER_FAIL = 'IMPORT_ORDER/POST_FAIL';

export default function importOrdersReducer(state = { repos: [] }, action) {
  switch (action.type) {
    case GET_IMPORT_ORDERS:
      return { ...state, loading: true };
    case GET_IMPORT_ORDERS_SUCCESS:
      return { ...state, loading: false, repos: action.payload };
    case GET_IMPORT_ORDERS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };

    case GET_IMPORT_ORDER:
      return { ...state, loading: true };
    case GET_IMPORT_ORDER_SUCCESS:
      return { ...state, loading: false, repo: action.payload };
    case GET_IMPORT_ORDER_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };

    case POST_IMPORT_ORDER:
      return { ...state, loading: true };
    case POST_IMPORT_ORDER_SUCCESS:
      return { ...state, loading: false, repo: action.payload };
    case POST_IMPORT_ORDER_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}
