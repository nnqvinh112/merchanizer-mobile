export const GET_PRODUCTS = 'PRODUCTS/LOAD';
export const GET_PRODUCTS_SUCCESS = 'PRODUCTS/LOAD_SUCCESS';
export const GET_PRODUCTS_FAIL = 'PRODUCTS/LOAD_FAIL';

export const GET_BEST_SELLERS = 'BEST_SELLERS/LOAD';
export const GET_BEST_SELLERS_SUCCESS = 'BEST_SELLERS/LOAD_SUCCESS';
export const GET_BEST_SELLERS_FAIL = 'BEST_SELLERS/LOAD_FAIL';

export default function productReducer(state = { repos: [] }, action) {
  switch (action.type) {
    case GET_PRODUCTS:
      return { ...state, loading: true };
    case GET_PRODUCTS_SUCCESS:
      return { ...state, loading: false, repos: action.payload };
    case GET_PRODUCTS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };

    case GET_BEST_SELLERS:
      return { ...state, loading: true };
    case GET_BEST_SELLERS_SUCCESS:
      return { ...state, loading: false, repos: action.payload };
    case GET_BEST_SELLERS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };

    default:
      return state;
  }
}