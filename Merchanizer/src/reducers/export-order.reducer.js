export const GET_EXPORT_ORDERS = 'EXPORT_ORDERS/LOAD';
export const GET_EXPORT_ORDERS_SUCCESS = 'EXPORT_ORDERS/LOAD_SUCCESS';
export const GET_EXPORT_ORDERS_FAIL = 'EXPORT_ORDERS/LOAD_FAIL';

export const GET_EXPORT_ORDER = 'EXPORT_ORDER/LOAD';
export const GET_EXPORT_ORDER_SUCCESS = 'EXPORT_ORDER/LOAD_SUCCESS';
export const GET_EXPORT_ORDER_FAIL = 'EXPORT_ORDER/LOAD_FAIL';

export const POST_EXPORT_ORDER = 'EXPORT_ORDER/POST';
export const POST_EXPORT_ORDER_SUCCESS = 'EXPORT_ORDER/POST_SUCCESS';
export const POST_EXPORT_ORDER_FAIL = 'EXPORT_ORDER/POST_FAIL';

export default function exportOrdersReducer(state = { repos: [] }, action) {
  switch (action.type) {
    case GET_EXPORT_ORDERS:
      return { ...state, loading: true };
    case GET_EXPORT_ORDERS_SUCCESS:
      return { ...state, loading: false, repos: action.payload };
    case GET_EXPORT_ORDERS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };

    case GET_EXPORT_ORDER:
      return { ...state, loading: true };
    case GET_EXPORT_ORDER_SUCCESS:
      return { ...state, loading: false, repo: action.payload };
    case GET_EXPORT_ORDER_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };

    default:
      return state;
  }
}
