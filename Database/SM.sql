CREATE DATABASE SALEMANAGEMENT
USE SALEMANAGEMENT

CREATE TABLE Customer(
IdCus char(4),
NameCus text,
Gender char(3),
Phone text,
Identify text,
Address text,
Email text,
Birthday datetime,
LastVisitedTime datetime,
primary key (IdCus),
)

------------------------------------

CREATE TABLE Supplier(
IdSup char(4),
Name text,
address text,
Phone text,
Taxcode int,
primary key (IdSup),
)

------------------------------------

CREATE TABLE Product(
IdPro char(4),
NamePro nvarchar(50),
Price money,
Quantity int,
Image image,
Category char(4),
primary key(IdPro),
CONSTRAINT FK_PRO_CATE FOREIGN KEY (Category) REFERENCES Category(IdCa),
)

--------------------------------------

CREATE TABLE Employee(
IdEm char(4),
NameEm text,
phone text,
indentify text,
address text,
role text,
primary key (IdEm),
)

--------------------------------------

CREATE TABLE Category(
IdCa char(4),
NameCa text,
primary key(IdCa),
)

------------------------------------

CREATE TABLE ExportOrder(
IdEO char(4),
TotalAmount int,
ToltalDiscount money,
TotalCost money,
CreatedBy char(4),
AppliedPromotions char(4),
StatusEO text,
primary key(IdEO),
CONSTRAINT FK_EO_PROM FOREIGN KEY (AppliedPromotions) REFERENCES Promotion(IdProm),
CONSTRAINT FK_EO_EM FOREIGN KEY (CreatedBy) REFERENCES Employee(IdEm),
)

------------------------------------

CREATE TABLE EODetail(
InfoProduct char(4),
TotalAmount int,
TotalDiscount money,
TotalCost money,
CONSTRAINT FK_EOD_PRO FOREIGN KEY (InfoProduct) REFERENCES Product(IdPro),
)

-------------------------------------

CREATE TABLE Account(
Username char(50),
Password text,
NameAc text,
Email text,
primary key(Username),
)

----------------------------------------

CREATE TABLE Promotion(
IdProm char(4),
NameProm text,
Description text,
Discount int,
DiscountedProduct char(4),
StartTime date,
EndTime date,
primary key(IdProm),
CONSTRAINT FK_PROM_PRO FOREIGN KEY (DiscountedProduct) REFERENCES Product(IdPro),
)

-----------------------------------------

CREATE TABLE ImportOrder(
IdIO char(4),
TotalCost money,
TotalAmount int,
Status text,
CreatedBy char(4),
SuppliedBy char(4),
primary key(IdIO),
CONSTRAINT FK_IO_Em FOREIGN KEY (CreatedBy) REFERENCES Employee(IdEm),
CONSTRAINT FK_IO_Sup FOREIGN KEY (SuppliedBy) REFERENCES Supplier(IdSup),
)

--------------------------------------------------

CREATE TABLE IODetail(
InfoProduct char(4),
TotalAmount int,
TotalCost money,
CONSTRAINT FK_IOD_PRO FOREIGN KEY (InfoProduct) REFERENCES Product(IdPro),
)
