﻿--CREATE DATABASE SALEMANAGEMENT
--USE SALEMANAGEMENT

use Merchanizer
go

CREATE TABLE Customer(
	IdCus char(4),
	NameCus text,
	Gender char(3),
	Phone text,
	Identify text,
	Address text,
	Email text,
	Birthday datetime,
	LastVisitedTime datetime,
	CreatedTime datetime,
	UpdatedTime datetime,
	primary key (IdCus),
)

insert into Customer(IdCus,NameCus,Gender,Phone,Identify,Address,Email,Birthday,LastVisitedTime,CreatedTime,UpdatedTime) values 
('C01',N'Nguyễn Văn An','Nam', '01223288411','0102912827',N'Nhà Bè, TPHCM','nguyenvanan@gmail.com','05/12/1890','','',''),
('C02',N'Nguyễn Ái Châu',N'Nữ', '01223288421','0102912832',N'Quận 8, TPHCM','nguyenaichau@gmail.com','05/11/1892','','',''),
('C03',N'Nguyễn Văn Tài','Nam', '01223232411','0102232827',N'Quận 7, TPHCM','nguyenvantai@gmail.com','05/04/1990','','',''),
('C04',N'Võ Văn Hùng','Nam', '01223288422','0102920002',N'Quận 9, TPHCM','vovanhung@gmail.com','06/10/1990','','',''),
('C05',N'Cao Ngọc Như Quỳnh',N'Nữ', '0589868880','0102923299',N'Cần Giuộc, Long An','caongocnhuquynh@gmail.com','06/03/1997','','',''),
('C06',N'Nguyễn Văn Ninh','Nam', '01227283288','0102326272',N'Cần Giuộc, Long An','nguyenvanninh@gmail.com','12/02/1997','','',''),
('C07',N'Phan Thị An Nhiên',N'Nữ', '01273288201','0102954932',N'Nhà Bè, TPHCM','ptan@gmail.com','05/12/1990','','',''),
('C08',N'Trần Hồng Ngọc',N'Nữ', '01223282938','0127278399',N'Quận 9, TPHCM','thn@gmail.com','01/02/1991','','',''),
('C09',N'Nguyễn Văn Hậu','Nam', '01213288232','0102383828',N'Châu Thành, Tiền Giang','nvhau@gmail.com','12/02/1890','','',''),
('C10',N'Lê Trung Hiếu','Nam', '01223229828','2328328289',N'Quận 1, TPHCM','lthieu@gmail.com','05/10/1992','','','')



------------------------------------

CREATE TABLE Supplier(
IdSup char(4),
Name text,
address text,
Phone text,
Taxcode int,
CreatedTime datetime,
UpdatedTime datetime,
primary key (IdSup),
)

insert into Supplier(IdSup,Name,address,Phone,Taxcode,CreatedTime,UpdatedTime) values
('S01',N'CTTNHH An Thành',N'Quận 7, TPHCM','0926326711','1122','',''),
('S02',N'CTTNHH Tân Hiệp Phát',N'Quận Bình Tân, TPHCM','0823662718','1452','',''),
('S03',N'CTCP Bibica',N'Quận 10, TPHCM','092982434','1392','',''),
('S04',N'CTCP Nước Giải Khát Chương Dương',N'Quận 8, TPHCM','0922224211','1432','',''),
('S05',N'CTTNHH TopCake',N'Cần Giuộc, Long An','0928388911','1299','','')

--------------------------------------

CREATE TABLE Category(
IdCa char(4),
NameCa text,
CreatdTime datetime,
UpdatedTime datetime,
primary key(IdCa),
)
insert into Category(IdCa,NameCa,CreatdTime,UpdatedTime) values
('Ca01','Nước Giải Khát','',''),
('Ca02','Bánh Kẹo','',''),
('Ca03','Thức ăn nhanh','',''),
('Ca04','Sữa','','')


------------------------------------

CREATE TABLE Product(
IdPro char(4),
NamePro nvarchar(50),
Price money,
Quantity int,
Image image,
Category char(4),
CreatedTime datetime,
UpdatedTime datetime,
primary key(IdPro),
CONSTRAINT FK_PRO_CATE FOREIGN KEY (Category) REFERENCES Category(IdCa),
)
insert into Product(IdPro,NamePro,Price,Quantity,Image,Category, CreatedTime,UpdatedTime) values
('P01','CoCa','8000','1000','','Ca01','',''),
('P02','PepSi','9000','1000','','Ca01','',''),
('P21','Lavie','6000','2000','','Ca01','',''),
('P03','Aquatina','6000','1500','','Ca01','',''),
('P04','Sting','10000','1000','','Ca01','',''),
('P05','Olong','8000','1500','','Ca01','',''),
('P06','Numberone','9000','1000','','Ca01','',''),
('P07',N'Sữa Milo','6000','1500','','Ca04','',''),
('P08',N'Sữa ông thọ mini','2000','500','','Ca04','',''),
('P09',N'Sữa Vinamilk','5500','2000','','Ca04','',''),
('P10',N'Sữa chua Vinamilk','6500','2200','','Ca04','',''),
('P11',N'Kẹo bốn mùa','5000','1000','','Ca02','',''),
('P12',N'Kẹo Chuppachup','2000','2000','','Ca02','',''),
('P13',N'Kẹo Sugus','4000','2000','','Ca02','',''),
('P14',N'Kẹo Oishi','6000','2000','','Ca02','',''),
('P15',N'Mì Omachi','8000','1000','','Ca03','',''),
('P16',N'Mì Hảo Hảo','4000','2000','','Ca03','',''),
('P17',N'Mì Gấu đỏ','4000','1000','','Ca03','',''),
('P18',N'Mì Aone','6000','1000','','Ca03','',''),
('P19',N'Mì ly Modern','7000','1500','','Ca03','',''),
('P20',N'Mì ly Cung Đình','7000','1000','','Ca03','','')
--------------------------------------

CREATE TABLE Employee(
IdEm char(4),
NameEm text,
Phone text,
Indentify text,
Address text,
Role text,
CreatedTime datetime,
UpdatedTime datetime,
primary key (IdEm),
)
insert into Employee(IdEm,NameEm,Phone,Indentify,Address,Role,CreatedTime,UpdatedTime) values
('E01','Cao Ngọc Quỳnh Như','0923726652','0293876252','Cần Giuộc, Long An',N'Nhân viên','',''),
('E02','Lâm Cao Lãnh','0948726652','0293439252','Quận 7, TPHCM',N'Nhân viên','',''),
('E03','Lê Ngọc Quang Vinh','0927467282','0293873729','Quận 7, TPHCM',N'Admin','','')

------------------------------------

CREATE TABLE ExportOrder(
IdEO char(4),
TotalAmount int,
TotalDiscount money,
TotalCost money,
CreatedBy char(4),
StatusEO text,
CreatedTime datetime,
UpdatedTime datetime,
primary key(IdEO),
CONSTRAINT FK_EO_EM FOREIGN KEY (CreatedBy) REFERENCES Employee(IdEm),
)
insert into ExportOrder(IdEO,TotalAmount,TotalDiscount,TotalCost,CreatedBy,StatusEO,CreatedTime,UpdatedTime) values
('Ex1','2','','16000','E01',N'Hoàn thành','',''),
('Ex2','3','','18000','E01',N'Hoàn thành','',''),
('Ex3','1','','10000','E02',N'Hoàn thành','','')

----------------------------------------

CREATE TABLE Promotion(
IdProm char(4),
NameProm text,
Description text,
DiscountValue int,
DiscountedProduct char(4),
DiscountType text,
StartTime date,
EndTime date,
CreatedTime datetime,
UpdatedTime datetime,
primary key(IdProm),
CONSTRAINT FK_PROM_PRO FOREIGN KEY (DiscountedProduct) REFERENCES Product(IdPro),
)

insert into Promotion(IdProm,NameProm,Description,DiscountValue,DiscountedProduct,DiscountType,StartTime,EndTime,CreatedTime,UpdatedTime) values
('Pr1',N'Khuyến mãi dịp hè','','10','P04','','06/01/2019','06/15/2019','',''),
('Pr2',N'Mua 1 tặng 1','','10','P04','','06/01/2019','06/15/2019','','')

------------------------------------

CREATE TABLE EODetail(
IdEO char(4),
InfoProduct char(4),
Amount int,
Discount money,
Cost money,
AppliedPromotions char(4),
CreatedTime datetime,
UpdatedTime datetime,
primary key(IdEO,InfoProduct),
CONSTRAINT FK_EOD_EO FOREIGN KEY (IdEO) REFERENCES ExportOrder(IdEO),
CONSTRAINT FK_EOD_PROM FOREIGN KEY (AppliedPromotions) REFERENCES Promotion(IdProm),
CONSTRAINT FK_EOD_PRO FOREIGN KEY (InfoProduct) REFERENCES Product(IdPro),
)
insert into EODetail(IdEO,InfoProduct,Amount,Discount,Cost,AppliedPromotions,CreatedTime,UpdatedTime) values
('Ex1','P01','2','','16000','Pr1','',''),
('Ex2','P01','3','','18000','Pr1','',''),
('Ex3','P01','1','','10000','Pr1','','')

-------------------------------------

CREATE TABLE Account(
Username char(50),
Password text,
NameAc text,
Email text,
CreatedTime datetime,
UpdatedTime datetime,
primary key(Username),
)
insert into Account(Username,Password,NameAC,Email,CreatedTime,UpdatedTime) values 
('','','','','',''),

-----------------------------------------

CREATE TABLE ImportOrder(
IdIO char(4),
TotalAmount int,
TotalCost money,
StatusIO text,
CreatedBy char(4),
SuppliedBy char(4),
CreatedTime datetime,
UpdatedTime datetime,
primary key(IdIO),
CONSTRAINT FK_IO_Em FOREIGN KEY (CreatedBy) REFERENCES Employee(IdEm),
CONSTRAINT FK_IO_Sup FOREIGN KEY (SuppliedBy) REFERENCES Supplier(IdSup),
)
insert into ImportOrder(IdIO,TotalAmount,TotalCost,StatusIO,CreatedBy,SuppliedBy,CreatedTime,UpdatedTime) values
('I01','200','10000000',N'Hoàn thành','E02','S02','',''),
('I02','500','34000000',N'Hoàn thành','E02','S03','',''),
--------------------------------------------------

CREATE TABLE IODetail(
IdIO char(4),
InfoProduct char(4),
Amount int,
Cost money,
Primary key (IdIO,InfoProduct),
CreatedTime datetime,
UpdatedTime datetime,
CONSTRAINT FK_IOD_PRO FOREIGN KEY (InfoProduct) REFERENCES Product(IdPro),
CONSTRAINT FK_IOD_IO FOREIGN KEY (IdIO) REFERENCES ImportOrder(IdIO),
)
insert into IODetail(IdIO,InfoProduct,Amount,Cost,CreatedTime,UpdatedTime) values
('I01','P05','200','10000000','',''),
('I02','P04','500','34000000','','')